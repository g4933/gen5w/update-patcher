#!/bin/bash

USB_PATH=$(dirname $0)
cd $USB_PATH

LOOP_OUTPUT=./loop_output.txt
printf "\n----- [LOOP_START] Caller:($(ps -o comm= $PPID)) - PID:($PPID) - At($(date)) [LOOP_START] -----\n" >> $LOOP_OUTPUT ;
printf "\n\n\n--- [LOOP_EXECUTION_START] Caller:($(ps -o comm= $PPID)) - PID:($PPID) - At($(date)) [LOOP_EXECUTION_START] --- \n" >> $LOOP_OUTPUT ;
touch /update/disableEncryptionUpdate
echo "touch /update/disableEncryptionUpdate" >> $LOOP_OUTPUT 2>&1 ;
printf "\n--- [LOOP_EXECUTION_END] Caller:($(ps -o comm= $PPID)) - PID:($PPID) - At($(date)) [LOOP_EXECUTION_END] --- \n\n\n" >> $LOOP_OUTPUT ;
sleep 10
