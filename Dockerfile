# To run this you need to build it with docker build --progress=plain -t update-patcher .
# then you can run it with "mkdir -p ./output && docker run -it -v $(pwd)/output:/workdir/output update-patcher" this will create an output folder which is the place where the new mango-rootfs.tar.gz will be places
FROM alpine
RUN apk update && apk add bash pv tar rsync
WORKDIR /workdir
ENV PATH_TO_WORKDIR="/workdir"
ENV UPDATE_DIR=${PATH_TO_WORKDIR}/update
ENV OUTPUT_DIR=${UPDATE_DIR}/output
COPY ./update_creator.sh ./
COPY ./update_patcher.sh ./
RUN chmod +x ./update_creator.sh
RUN chmod +x ./update_patcher.sh
RUN chown -R 1000:1000 /workdir
USER 1000
CMD ["/workdir/update_patcher.sh"]
