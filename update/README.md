You must place here all the update files, as if it were your "root" of your usb. Currently only `mango-rootfs.tar.gz` is really "supported", the only reason for that is that it's what achieves our persistency, tho you can adapt this to make it modify other stuff


# Folder structure
```
update
│   README.md
│   mango-rootfs-tar.gz <-- You must provide this file yourself, it must be already decyrpted!    
│
└───mango-rootfs
    │   ...
    │
    └───etc
        │   ...
        │   
        └───systemd
            │   ...
            │
            └───system
                │   wideopen.service <-- You can provide your own, but if this file doesn't exist, it will be downloaded automatically from the latest version
                │   ...
```