#!/bin/bash

mkdir -p mango-rootfs
cd mango-rootfs

# Creates a new mango-rootfs
tar --format=ustar --numeric-owner -cf - ./ -P | pv -s $(du -sb ./ | awk '{print $1}') -n | gzip > ../mango-rootfs.tar.gz
