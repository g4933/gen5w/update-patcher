#!/bin/bash

export PATH_TO_WORKDIR="/workdir"
export UPDATE_DIR=${PATH_TO_WORKDIR}/update
export OUTPUT_DIR=${UPDATE_DIR}/output

mkdir -p ${UPDATE_DIR}
if [[ -d "${OUTPUT_DIR}" ]]; then
    echo "(Please, wait...) Cleaning up existing output dir..."
    rm -rf ${OUTPUT_DIR} | true # Cleanup the output dir before starting to work on it, ignore failures
    mkdir -p ${OUTPUT_DIR}
    touch ${OUTPUT_DIR}/.gitkeep
fi

# Backup original update file
echo "(Please, wait...) Backup original update file"
cp ${UPDATE_DIR}/mango-rootfs.tar.gz ${UPDATE_DIR}/mango-rootfs.tar.gz.bak

# Uncompress the tar, but keep it in tar because we only need to patch it, not replace it completely
echo "(Please, wait...) Untar mango-rootfs.tar.gz"
mkdir -p ${PATH_TO_WORKDIR}/mango-rootfs-original
pv ${UPDATE_DIR}/mango-rootfs.tar.gz | tar -xzf - -C "${PATH_TO_WORKDIR}/mango-rootfs-original"

# Go to the update folder
cd ${UPDATE_DIR}

# Make the service folder where we will place wideopen.service in case it wasn't already present on the directory
mkdir -p ${UPDATE_DIR}/mango-rootfs/etc/systemd/system/

# Download the latest payload (Command will fail if one already exists but we ignore the failure assuming you intended to do yuour own wideopen.service)
echo "(Please, wait...) Try download latest wideopen.service"
wget https://gitlab.com/g4933/gen5w/navi_extended/-/raw/main/USB_FILES/wideopen.service -P ${UPDATE_DIR}/mango-rootfs/etc/systemd/system/ | true

# Install the service
mkdir -p ${UPDATE_DIR}/mango-rootfs/etc/systemd/system/multi-user.target.wants/
cd ${UPDATE_DIR}/mango-rootfs/etc/systemd/system/multi-user.target.wants/
ln -s /etc/systemd/system/wideopen.service wideopen.service
cd -

# Swap the folders for more efficient data operation (otherwise we'd copy too much)
cp -R ${UPDATE_DIR}/mango-rootfs ${PATH_TO_WORKDIR}/mango-rootfs-new-additions

# Make the og as mango-rootfs, we will then move all files from the new additions to this one replacing all 
mv ${PATH_TO_WORKDIR}/mango-rootfs-original ${PATH_TO_WORKDIR}/mango-rootfs
rsync -av ${PATH_TO_WORKDIR}/mango-rootfs-new-additions/ ${PATH_TO_WORKDIR}/mango-rootfs/

# We go into the folder because the file paths MUST be relative, exactly as they are originally (./)
cd ${PATH_TO_WORKDIR}/mango-rootfs

# Turn off Engineering and Dealer mode key check
cd ./app/share/
find AppEngineerMode -type f -name "AppEngineerMode_PinCodeKeypad.qml" | xargs sed -i 's/enterMenu\ ==\ 21/enterMenu/g'
find AppEngineerMode -type f -name "AppEngineerMode_PinCodeKeypad.qml" | xargs sed -i 's/enterMenu\ ==\ 11/enterMenu/g'
cd -

# Patch some files for more engineering accessiblity
cd ./app/share/
find . -type f -name "*.qml" | xargs sed -i 's/checkSOPVersion() ===/checkSOPVersion() !==/g'
find . -type f -name "*.qml" | xargs sed -i 's/checkSOPVersion()===/checkSOPVersion()!==/g'
find . -type f -name "*.qml" | xargs sed -i 's/: AppEngineerModeEngine.checkSOPVersion()/: false/g'
cd -

# Gotta bring back dropbear gosh darn it
if [[ ! -f "/usr/sbin/dropbear" ]]; then
    cd ./usr/sbin/
    ln -s ./dropbearmulti ./dropbear
    cd -
fi

# Append to existing mango-rootfs
echo "(Please, wait...) Compress the newly modified mango-rootfs to mango-rootfs.tar.gz"
tar --format=ustar --exclude="*.DS_Store" --numeric-owner -cf - ./ -P | pv -s $(du -sb ./ | awk '{print $1}') | gzip > ${OUTPUT_DIR}/mango-rootfs.tar.gz

# Put the backup mango tar back just in case the original was destroyed by a name collision
rm -rf ${UPDATE_DIR}/mango-rootfs/
rm ${UPDATE_DIR}/mango-rootfs.tar.gz 
mv ${UPDATE_DIR}/mango-rootfs.tar.gz.bak ${UPDATE_DIR}/mango-rootfs_original.tar.gz
