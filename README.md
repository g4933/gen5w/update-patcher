# How does it work?
Update files can be patched to install things like a patched AppNavi (to continue to allow the `wideopen_service` to run) or different map files.

To allow this the file `/update/disableEncryptionUpdate` must exist; if it is found, `AppUpgrade` will perform the update without encryption.

## Benefits?
* You can customize your OS
* The update is MUCH MUCH MUCH faster!

## How to install `/update/disableEncryptionUpdate`
*before performing these steps make sure you've installed* `navi_extended` *from* [here](https://gitlab.com/g4933/gen5w/navi_extended)
1. Download the `disable_encryption.sh` script locally and rename it to `main_loop.sh`
1. Copy to exFAT formatted USB drive
1. Plug into car and wait for script to execute, should take no longer than X minutes.

## Patching existing update to preinstall wideopen.service (MUST BE DECRYPTED AND UPDATES ON UNIT MUST BE DECRYPTED AS WELL! )
Note: Your unit must already be modified for this! this should help you update to newer versions keeping the service installed WITHOUT having to find a new exploit.

1. Make sure you have `mango-rootfs.tar.gz` (decrypted) on the folder `/update`
2. Run the following on your terminal `docker compose build` and then `docker compose up` and wait a little
3. You will find the new mango-rootfs.tar.gz on `/update/output/mango-rootfs.tar.gz` 


## How to add stuff to the tars?
It is a bit of a pain adding things on the tar sometimes. This is because they use an old format, and it fails if it is not right... Maybe it was on my old testing due to something else missing... Not sure. In any case, if you have issues check below.

### To make tar.gz 
Go into the folder you want to compress and run the code below
```
tar --format=ustar --numeric-owner -cf - ./ -P | pv -s $(du -sb ./ | awk '{print $1}') -n | gzip > name_of_output.tar.gz
```

### To make a new .tar
Go into the folder you want to compress and run the code below
```
tar --format=ustar --numeric-owner -cf "name_of_output.tar" ./
```

### To add to existing .tar
Go into the folder you want to compress and run the code below
```
tar --format=ustar --no-fflags --numeric-owner -rvf "mango-name_of_output.tar" "file1" "file2" "path/to/file3" ...
```
